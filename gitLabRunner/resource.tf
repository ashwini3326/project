resource "aws_spot_instance_request" "GitLabRunner" {
	ami = "ami-03d315ad33b9d49c4"
  	spot_price    = "0.004"
	wait_for_fulfillment = true
	instance_type = "t2.micro"
	key_name = "GitLabRunner"
	security_groups = ["GitLabRunner"]
	tags = {
		Name = "GitLabRunner"
		}


	provisioner "remote-exec" {
        	connection{
                	host = self.public_ip 
                	type = "ssh"
                	private_key = file("GitLabRunner.pem")
                	user = "ubuntu"

                	}	
        	inline = [
			"sudo apt-get update",
			"sudo apt-get upgrade -y",
			"sleep 10",
			"sudo apt-get update -y",
			"sudo apt-get install docker.io docker-compose -y",

			"curl -LJO 'https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb' ",
			"sudo dpkg -i gitlab-runner_amd64.deb",
			"sudo usermod -aG docker gitlab-runner",
			"sudo gitlab-runner register --non-interactive --tag-list ec2_shell_runner --url https://gitlab.com/ -r zVWUu9qyPYFg1kNAZyTU --executor shell",
			"sudo gitlab-runner register --non-interactive --tag-list ec2_docker_runner --url https://gitlab.com/ -r zVWUu9qyPYFg1kNAZyTU --executor docker --docker-image alpine",
			"sleep 5; sudo gitlab-runner verify",
			"sudo mv /home/gitlab-runner/.bash_logout /home/gitlab-runner/#bash.logout#",
			]
	}
	provisioner "remote-exec"{
		on_failure = continue
		connection{
                        host = self.public_ip
                        type = "ssh"
                        private_key = file("GitLabRunner.pem")
                        user = "ubuntu"

                        }
		when = destroy
		inline = [
			"sudo gitlab-runner stop",
			"sudo gitlab-runner unregister --all-runners"
				]
				}	
}
