terraform {
	backend "s3" {
	bucket = "terraformstatesdirectory"
	region = "us-east-1"
    	key = "global/gitlab-runner/terraform.tfstate"
			}
}
