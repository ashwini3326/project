resource "aws_instance" "staging-server" {
	ami = "ami-03d315ad33b9d49c4"
	instance_type = "t2.micro"
	key_name = "staging-server"
	security_groups = ["staging-server"]
	tags = {
		Name = "staging-server"
		}


	provisioner "remote-exec" {
        	connection{
                	host = self.public_ip 
                	type = "ssh"
                	private_key = file("staging-server.pem")
                	user = "ubuntu"

                	}	
        	inline = [
			"sudo apt-get update -y",
			"sudo apt-get upgrade -y",
			"sudo apt-get update",
			"sudo apt-get install docker.io -y",
			"sudo usermod -aG docker ubuntu",

			]
	}
        provisioner "remote-exec" {
                connection{
                        host = self.public_ip
                        type = "ssh"
                        private_key = file("staging-server.pem")
                        user = "ubuntu"

                        }
                inline = [
                        "docker swarm init",

                        ]
        }
	provisioner "remote-exec"{
		on_failure = continue
		connection{
                        host = self.public_ip
                        type = "ssh"
                        private_key = file("staging-server.pem")
                        user = "ubuntu"

                        }
		when = destroy
		inline = [
			"echo deleting"
				]
				}	
}
