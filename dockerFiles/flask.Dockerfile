FROM	alpine:latest

COPY	flaskApp /flaskApp

RUN	apk update;\
	apk upgrade;\
	apk add py-pip;\
	cd flaskApp ; pip install -r requirements.txt
RUN	pwd
ENV FLASK_APP=flaskApp/main.py
ENTRYPOINT	["flask", "run"] 
CMD	["--host=0.0.0.0", "--port=5000"]


	
