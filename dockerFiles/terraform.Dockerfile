FROM	alpine
RUN 	wget -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/0.14.6/terraform_0.14.6_linux_amd64.zip
RUN	apk add ca-certificates
RUN	unzip /tmp/terraform.zip -d /bin
#ENTRYPOINT	["/terraform"]

